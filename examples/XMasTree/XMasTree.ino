#include <LEDColor.h>
#include <LEDStripLibrary.h>

/*
 * A XMas Tree consists of 3 LED-circles (rings). Each circle consists of a particular cound of WD2811 LED Devices.
 *
 * - The Topmost ring contains 3 LEDs in total
 * - The middle ring contains 12 LEDs in total
 * - The bottom ring contains 16 LEDs in total
 *
 * All three rings are chained together that they become a LED-Strip of 3 + 12 + 16 LEDs in total.
 */
static const uint8_t kNrOfLEDs = 3+12+16;
static const uint8_t kDimBright =  2;
static const uint8_t kMaxBright = 50;

void setup()
{}

uint16_t hue = 0;

static void hueCycle(LEDStrip& ledStrip, uint8_t iterations = 8)
{
    for(uint8_t iter = iterations; iter;--iter)
    {
        for (uint8_t i = 0; i < ledStrip.nrOfLeds(); ++i)
        {
            const LEDColor color(LEDColor::colorWithHSV(hue, 255, kMaxBright));
            ledStrip[i] = color;
        }

        ledStrip.commit();
        _delay_ms(30);

        hue++;
        if (hue >= 360)
            hue = 0;
    }
}

static void hueCycle2(LEDStrip& ledStrip, uint8_t iterations = 8)
{
    for(uint8_t iter = iterations; iter;--iter)
    {
        for (uint8_t bright=0; bright != ledStrip.nrOfLeds();++bright)
        {
            for (uint8_t i = 0; i != ledStrip.nrOfLeds(); ++i)
            {
                ledStrip[i] = LEDColor::colorWithHSV(hue, 255, i==bright ? kMaxBright : kDimBright);
            }

            ledStrip.commit();
            _delay_ms(30);
        }


        ++hue;
        if (hue >= 360)
            hue = 0;
    }
}

static void sparkle(LEDStrip& ledStrip, uint8_t iterations = 8)
{
    for(uint8_t iter = iterations; iter;--iter)
    {
        for (uint8_t i = 0; i != ledStrip.nrOfLeds(); ++i)
        {
            const bool sparkle = (rand() & 0b1111) == 3;
            ledStrip[i] = LEDColor::colorWithHSV(hue, sparkle ? 128 : 255, sparkle ? kMaxBright : kDimBright);
        }

        ledStrip.commit();
        _delay_ms(100);


        ++hue;
        if (hue >= 360)
            hue = 0;
    }
}

static void sparkle1(LEDStrip& ledStrip, uint8_t iterations = 8)
{
    for(uint8_t iter = iterations; iter;--iter)
    {
        for (uint8_t i = 0; i != ledStrip.nrOfLeds(); ++i)
        {
            const bool sparkle = (rand() & 0b111) == 3;
            if (sparkle) {
                ledStrip[i] = LEDColor::colorWithHSV(rand() % 360, 255, kMaxBright);
            } else {
                ledStrip[i] = LEDColor::colorWithHSV(hue, 255, kDimBright);
            }
        }

        ledStrip.commit();
        _delay_ms(100);


        ++hue;
        if (hue >= 360)
            hue = 0;
    }
}

static void sparkle2(LEDStrip& ledStrip, uint8_t iterations = 8)
{
    for(uint8_t iter = iterations; iter;--iter)
    {
        for (uint8_t i = 0; i != ledStrip.nrOfLeds(); ++i)
        {
            const bool sparkle = (rand() & 0b111) == 3;
            if (sparkle) {
                ledStrip[i] = LEDColor::colorWithHSV(hue+180, 255, kMaxBright);
            } else {
                ledStrip[i] = LEDColor::colorWithHSV(hue, 255, kDimBright);
            }
        }

        ledStrip.commit();
        _delay_ms(100);


        ++hue;
        if (hue >= 360)
            hue = 0;
    }
}

static void lightstair1(LEDStrip& ledStrip, uint8_t iterations = 8)
{
    uint8_t stairZero=0;

    for(uint8_t iter = iterations; iter;--iter)
    {
        for (uint8_t i = 0; i != ledStrip.nrOfLeds(); ++i)
        {
            uint8_t value;
            if (i < stairZero)
            {
                value = 0;
            }
            else
            {
                value = i-stairZero;
            }

            ledStrip[i] = LEDColor::colorWithHSV(hue, 255, value);
        }
        ++stairZero;
        ledStrip.commit();
        _delay_ms(50);

        ++hue;
        if (hue >= 360)
            hue = 0;
    }
}

static void lightstair2(LEDStrip& ledStrip, uint8_t iterations = 8)
{
    uint8_t stairOffset=0;

    for(uint8_t iter = iterations; iter;--iter)
    {
        for (uint8_t i = 0; i != ledStrip.nrOfLeds(); ++i)
        {
            const uint8_t ic = (i+stairOffset) % (2*kMaxBright);
            int8_t value(ic<kMaxBright ? ic : 2*kMaxBright-ic);

            ledStrip[i] = LEDColor::colorWithHSV(hue, 255, value);
        }

        ++stairOffset;
        ledStrip.commit();
        _delay_ms(50);

        ++hue;
        if (hue >= 360)
            hue = 0;
    }
}

static void lightSpiral1(LEDStrip& ledStrip, uint8_t iterations = 8)
{
    for(uint8_t iter = iterations; iter;--iter)
    {
        for (uint8_t i = 0; i != ledStrip.nrOfLeds(); ++i)
        {
            ledStrip[i] = LEDColor::colorWithHSV(hue+10*(iter+i), 255, kMaxBright);
        }

        ledStrip.commit();
        _delay_ms(100);
    }
    ++hue;
    if (hue >= 360)
        hue = 0;
}

class CLEDRing
{
    LEDStrip& m_Strip;
    uint8_t m_FromIndex;
    uint8_t m_ToIndex;

 public:
    CLEDRing(LEDStrip& inStrip, uint8_t inFromIndex, uint8_t inToIndex)
    : m_Strip(inStrip)
    , m_FromIndex(inFromIndex)
    , m_ToIndex(inToIndex)
    {}

    uint8_t nrOfLeds() const {return m_ToIndex - m_FromIndex + 1;}

    LEDColor& operator[](int8_t inIndex)
    {
        inIndex %= nrOfLeds();

        if (inIndex < 0)
            inIndex += nrOfLeds();

        return m_Strip[m_FromIndex+inIndex];
    }

    void rotateBy(int8_t inIndex)
    {
        if (inIndex)
        {
            inIndex %= nrOfLeds();

            bool clockwise = true;
            if (inIndex < 0)
            {
                clockwise = false;
                inIndex = -inIndex;
            }

            for (;inIndex;--inIndex) {
                _rotateBy1(clockwise);
            }
        }
    }

    void setAllToColor(const LEDColor& inColor)
    {
        const LEDColor* dstPtr = &m_Strip[m_FromIndex];
        for (uint8_t i=0; i!= nrOfLeds(); ++i)
        {
            *(dstPtr++) = inColor;
        }
    }

    void blank()
    {
        setAllToColor(LEDColor(0,0,0));
    }

private:
    void _rotateBy1(bool clockwise)
    {
        if (!clockwise)
        {
            const LEDColor* dstPtr = &m_Strip[m_FromIndex];
            const LEDColor tmpColor(*dstPtr);

            for (uint8_t i=nrOfLeds(); i; --i)
            {
                *(dstPtr++) = *(dstPtr+1);
            }
            m_Strip[m_FromIndex + nrOfLeds()-1] = tmpColor;
        }
        else
        {
            const LEDColor* dstPtr = &m_Strip[m_FromIndex + nrOfLeds()-1];
            const LEDColor tmpColor(*dstPtr);

            for (uint8_t i=nrOfLeds(); i; --i)
            {
                *(dstPtr--) = *(dstPtr-1);
            }
            m_Strip[m_FromIndex + 0] = tmpColor;
        }
    }
}; // class CLEDRing

class XMasTree
{
    LEDStrip m_LedStrip;
    CLEDRing m_Ring1;
    CLEDRing m_Ring2;
    CLEDRing m_Ring3;

public:
    XMasTree()
    : m_LedStrip(kNrOfLEDs, PORTB, PB0)
    , m_Ring1(m_LedStrip, 0,    16-1)
    , m_Ring2(m_LedStrip, 16,   16+12-1)
    , m_Ring3(m_LedStrip, 16+12,16+12+3-1)
    {
        m_LedStrip.begin();
    }

    CLEDRing& ring(uint8_t inIndex) {
        switch(inIndex){
            case 0 : return m_Ring1;
            case 1 : return m_Ring2;
            default: // intentional Fall through
            case 2 : return m_Ring3;
        }
    }

    LEDStrip& ledStrip() {return m_LedStrip;}

    void commit(){
        m_LedStrip.commit();
    }

    void setAllToColor(const LEDColor& inColor)
    {
        for (uint8_t i=0; i!= m_LedStrip.nrOfLeds(); ++i)
        {
            m_LedStrip[i] = inColor;
        }
    }

    void blank()
    {
        setAllToColor(LEDColor(0,0,0));
    }
}; // class XMasTree

static void lightRing1(XMasTree& tree, uint8_t iterations = 8)
{
    uint8_t stairOffset=0;
    CLEDRing& ring(tree.ring(1));
    tree.blank();

    ring.setAllToColor(LEDColor::colorWithHSV(hue, 255, kMaxBright));

    ring[1] = mixedColor(ring[1], LEDColor::colorWithHSV(hue+180, 255, kMaxBright), 255);

    for(uint8_t iter = ring.nrOfLeds(); iter;--iter)
    {
        ring.rotateBy(-1);
        tree.commit();

        _delay_ms(500);
    }
}

void loop()
{
    XMasTree tree;

    for(;;) {
//        hueCycle2(tree.ledStrip(),4);
//        sparkle(tree.ledStrip(),32);
//        sparkle1(tree.ledStrip(),32);
//        sparkle2(tree.ledStrip(),32);
//        lightstair1(tree.ledStrip(),32);
//        lightstair2(tree.ledStrip(),32);
//        lightSpiral1(tree.ledStrip(),32);
        lightRing1(tree, 32);
    }
}
