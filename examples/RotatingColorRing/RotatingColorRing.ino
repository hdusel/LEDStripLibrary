#include <LEDColor.h>
#include <LEDStripLibrary.h>

LEDStrip ledStrip(100, PORTB, PB0); // Create an LED Strip object that consists of 100 LEDs

void setup()
{
    // Configure Pin that will be used as Data/Clock pin that drives the LED Strip.
    ledStrip.begin();
}

void loop()
{
	static uint16_t gHueOffset = 0;

    // Now assign every LED a separate color value (hue) that finally represents a "full circle".
    for (uint8_t i = 0; i != ledStrip.nrOfLeds(); ++i)
    {
        const uint16_t hue(gHueOffset + ((360*i)/ledStrip.nrOfLeds()));

        ledStrip[i] = LEDColor::colorWithHSV(
        hue % 360, // The Hue (color Value)
        255, // The Saturation set to full
        255); // The Value (brightness) of the color value
    }

    // commit all changes
    ledStrip.commit();

    // Wait while and then...
    _delay_ms(30);

    // ...increment the hue offset that finally gives the impression of a rotating color circle.
    ++gHueOffset;
}
