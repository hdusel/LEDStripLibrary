# Arduino LED Strip Library for WS8211 Multi Color LEDs

## Brief intro
This is a Library for the Arduino IDE that is capable to drive LED-Strips that are equipped with 'Smart-LED's` [WS8211].

A [WS8211] is actually a compound of 3 LEDs with the colors red, green and blue.

## Limitations and Hints

- The code of the Library is currently limited to AVR Mega chips. However the porting to AVR Tiny chis should no big deal either but demands some adaptations.
- Currently the Library relies on a Hardware that runs with a fixed clock frequency of 16 MHz.

Hint: An adaptation to a Hardware with a different clock frequency of 16 MHz requires an adjustment of the two Assembler routines ```LED_STRIP_PULSE0()``` and ```LED_STRIP_PULSE1()``` (LEDStripLibrary.cpp)

Currently for a Arduino Mega clocked at 16MHz this looks like:

~~~asm
#pragma GCC optimize ("O3")
/* ===================================================================
 * These two Functions shape the pulse for a WD2811 LED Driver.
 * Note that these are calculated for a Board which runs at a Clock Frequency
 * of 16 MHz!
 *
 * If your Board runs at another Clock then yo'll consider to cange these
 * two macros accordingly!
 * Not: It is vital that these two functions are compiled with optimization
 * turned on! That is because the set and clear mask  hast to be substituded
 * to ```sbi``` and ```cbi``` assembly instructions which will happen only of
 * optimization is active!
 * =================================================================== */
// A '0' is a pulse with a high time of 0.320 us and a low time of 0.8 us
inline static void LED_STRIP_PULSE0(volatile uint8_t& inPort, uint8_t inPortMask)
{
    // Turn on optimization to let the compiler create a
    // ```sbi _SFR_IO_ADDR(inPort),inPortBit```instruction!
    inPort |= inPortMask;
    __asm __volatile__(\
        /* 5 Clocks for high (0.375 us @ 16 MHz) */\
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
    );

    // Turn on optimization to let the compiler create a
    // ```cbi _SFR_IO_ADDR(inPort),inPortBit```instruction!
    inPort &= ~inPortMask;
    __asm __volatile__(\
        /* 14 Clocks for low (0.875 us @ 16 MHz) */\
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
    );
} __attribute__((optimize()))

// A '1' is a pulse with a high time of 0.65 us and a low time of 1 us
// A 'CAPT' is a pulse with a low time of > 50us
inline static void LED_STRIP_PULSE1(volatile uint8_t& inPort, uint8_t inPortMask)
{
    // Turn on optimization to let the compiler create a
    // ```sbi _SFR_IO_ADDR(inPort),inPortBit```instruction!
    inPort |= inPortMask;
    __asm __volatile__(\
        /* 10 Clocks for high (0.625 us @ 16 MHz) */\
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
    );

    // Turn on optimization to let the compiler create a
    // ```cbi _SFR_IO_ADDR(inPort),inPortBit```instruction!
    inPort &= ~inPortMask;
    __asm __volatile__(\
        /* 6 Clocks for low (0.35 us @ 16 MHz) */\
        "   nop\n"     /* 1 Clock */ \
    );
} __attribute__((optimize()))
~~~

## Example

The following example creates a LED Strip that is assumed to carry 100 LEDs meaning 100 x WS8211 devices which are connected in a daisy-chain.

This color circle then slowly turns upon every call of loop().

The whole strip then will light in rainbow color meaning that a full color hue circle range is spread along the 100 LEDs.

Or in other words: If the LED-Strip in would be placed as a circle then it reflects the real hue value according the circles color value.

~~~cpp
#include <LEDColor.h>
#include <LEDStripLibrary.h>

// Create an LED Strip object that consists of 100 LEDs.
// The LED Strip is driven by PortB, Pin PB0
LEDStrip ledStrip(100, PORTB, PB0);

void loop()
{
    static uint16_t gHueOffset = 0;

    // Now assign every LED a separate color value (hue) that finally represents a "full circle".
    for (uint8_t i = 0; i != ledStrip.nrOfLeds(); ++i)
    {
        const uint16_t hue(gHueOffset + ((360*i)/ledStrip.nrOfLeds()));

        ledStrip[i] = LEDColor::colorWithHSV(
        hue % 360, // The Hue (color Value)
        255, // The Saturation set to full
        255); // The Value (brightness) of the color value
    }

    // commit all changes
    ledStrip.commit();

    // Wait while and then...
    _delay_ms(30);

    // ...increment the hue offset that finally gives the impression of a rotating color circle.
    ++gHueOffset;
}
~~~

## References
- Documentation of the WS8211: [https://cdn-shop.adafruit.com/datasheets/WS2811.pdf](https://cdn-shop.adafruit.com/datasheets/WS2811.pdf)

[WS8211]: https://cdn-shop.adafruit.com/datasheets/WS2811.pdf

