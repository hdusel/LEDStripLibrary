#pragma once
// LEDColor.h
/*!
 * Description of a LED Color item.
 * This is actually a model of a color for single
 * WS2811 LED. Such LEDs consists of 3 basic colors tht are components namely
 * of red, green and blue.
 */

#if !defined(NO_ARDUINO)
#   include <Arduino.h>
#endif

#include <stdint.h>
#include <stdlib.h>
#include <avr/io.h>

#define _MIN(a,b) (((a)<(b))?(a):(b))
#define _MAX(a,b) (((a)>(b))?(a):(b))

/*!
 * Representation of a Color component. This class is a container that represents
 * a color that is described by the three basic color components _red_, _green_
 * and _blue_.
 *
 * The range for color componenets is 0(meaning 0%)...255(meaning 100%).
 *
 * However this class offers some convenient functions that allows to cope with
 * a HSV Color Model (H=Hue, S=Saturation, V=Value).
 *
 * \author "Hans-Peter Dusel"
 */
struct LEDColor
{
    uint8_t r; //!< Red color component
    uint8_t g; //!< Green color component
    uint8_t b; //!< Blue color component

	LEDColor()
	{}

    /*!
     * Creates a copy from another color component.
     * \param inColor The LED Color to copy from.
     */
    LEDColor(const LEDColor& inColor)
    : LEDColor(inColor.r, inColor.g, inColor.b){}

    /*!
     * Creates a new Color Value given by the three color components
     * _red_, _green_ and _blue_
     * \param inRed The _red_ color component of the new pixel in a range of [0...255].
     * Value above 0x255 will be clamped.
     * \param inGreen The _green_ color component of the new pixel in a range of [0...255].
     * Value above 0x255 will be clamped.
     * \param inBlue The _blue_ color component of the new pixel in a range of [0...255].
     * Value above 0x255 will be clamped.
     */
	LEDColor(uint8_t inRed, uint8_t inGreen, uint8_t inBlue)
	:r(_MIN(inRed, 255)), g(_MIN(inGreen, 255)), b(_MIN(inBlue, 255)){}

    /*!
     * Assigns another color value to this color
     * \param inRHS The right hand side color Value to assign to.
     \return this LEDColor object after the assignment.
     */
    LEDColor& operator=(const LEDColor& inRHS)
    {
        r = inRHS.r;
        g = inRHS.g;
        b = inRHS.b;
        return *this;
    }

    /*!
     * Construct a new LED Color with the basic colors red, green and blue.
     * \param inRed The _red_ color component with a range of [0...255]. 0 means dark, 255 means full light.
     * \param inGreen The _green_ color component with a range of [0...255]. 0 means dark, 255 means full light.
     * \param inBlue The _blue_ color component with a range of [0...255]. 0 means dark, 255 means full light.
     * \see LEDColor::colorWithHSV(uint16_t,uint8_t,uint8_t)
     */
    static LEDColor colorWithRGB(uint8_t inRed, uint8_t inGreen, uint8_t inBlue);

    /*!
     * Construct a new LED Color with the color model HSV (hue, saturation, value).
     * \param inHue The Hue value that represents an an angle in units of degree. Internally this value is considered as modulo 360.
     * \param inSaturation The Saturation of the color with a range from 0 (no saturation) ...255(full saturated)
     * \param InValue The _brightness_ of the color with a range from 0 (dark) ...255(full bright)
     * \see LEDColor::colorWithRGB(uint8_t,uint8_t,uint8_t)
     */
    static LEDColor colorWithHSV(int16_t inHue, uint8_t inSaturation, uint8_t inValue);

	/*!
	 * Convinient method to set the pixel according HSV color model.
     * \param inHue The Hue value that represents an an angle in units of degree. Internally this value is considered as modulo 360.
     * \param inSaturation The Saturation of the color with a range from 0 (no saturation) ...255(full saturated)
     * \param InValue The _brightness_ of the color with a range from 0 (dark) ...255(full bright)
	 */
    void setHSV(int16_t inHue, uint8_t inSaturation, uint8_t inValue) {
        *this = colorWithHSV(inHue, inSaturation, inValue);
    }

private:
    void _limitBrightness(uint8_t inBrightValue);
} __attribute__((packed)); // struct LEDColor

LEDColor mixedColor(const LEDColor& inColorA, const LEDColor& inColorB, uint8_t inWeighting=128);
