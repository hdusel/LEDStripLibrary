#pragma once
// File: LEDStripLibrary.h

#if !defined(NO_ARDUINO) && !defined(ARDUINO_ARCH_AVR)
#error "This library does support AVR only!"
#endif

#if !defined(F_CPU) || (F_CPU!=16000000)
#   error "The Board has to run with 16MHz. Your Board apparently does not!"
#endif

#include "LEDColor.h"

#if !defined(NO_ARDUINO)
#   include <Arduino.h>
#endif

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdlib.h>

/*!
 * Helper Class that temporarily disables the global interrupt flag if it is enabled.
 */
class ISRDisabler
{
    bool m_WasISRSet;

public:
    ISRDisabler()
    : m_WasISRSet(isI_SREGSet())
    {
        if (m_WasISRSet)
            cli();
    }

    ~ISRDisabler()
    {
        if (m_WasISRSet)
        {
            sei();
        }
    }

    inline static bool isI_SREGSet()
    {
        uint8_t sreg;

        __asm __volatile__(
            "in %0,__SREG__"
            :"=r"(sreg)
        );
        return (sreg & _BV(7));
    }

}; // class ISRDisabler

/*!
 * Manage a LED strip that consists of LEDs with WS2811/12 drivers.
 *
 * Example:
 * ~~~cpp
 *  // Perform a cycle of the hue value (the color value) by keeping the saturation
 *  // to "full" (255) and limit the brightness to 50% (128)
 *
 *  LEDStrip ledStrip(100); // Deal with 100 LEDs
 *
 *  for(;;)
 *  {
 *      static uint16_t hue = 0;
 *      LEDColor color(LEDColor::colorWithHSV(hue, 255, 128));
 *
 *      for (uint8_t i = ledStrip.nrOfLeds(); i; --i)
 *      {
 *          ledStrip[i] = color;
 *      }
 *
 *      ledStrip.commit();
 *      _delay_ms(10); // Wait 10 ms for the next cycle -> 360° * 10 ms / ° = 3.6 seconds for a full cycle
 *      hue++;
 *
 *      if (hue >= 360) // limit the angle
 *          hue = 0;
 *  }
 * ~~~
 *
 * \author "Hans-Peter Dusel"
 */
class LEDStrip
{
    uint16_t m_NrOfLeds; //!< Remember the number of LEDs
    volatile uint8_t& m_Port;
    uint8_t m_PortMask;
    LEDColor* m_Leds;    //!< An Array of Color Objects. Each Objects represents one LED.

public:

    /*!
     * Create a new LEDStrip Object given by a certain number of LEDs
     * \param inNrOfLeds The total number of LEDs this strip consists of.
     * \param inPort The port that drives the LED Strip.
     * \param inPortPin The Port Bit that drives the LED Strip
     */
    LEDStrip(uint16_t inNrOfLeds, volatile uint8_t& inPort, uint8_t inPortPin);

    /*!
     * Destroys this LEDStrip Object and releases all resources which has been allocated with it.
     */
    ~LEDStrip();

    /*!
     * Return the number of LEDs for this Strip.
     * That is the number as handed over in the ctor.
     * \return the number of LEDs this Strip consists of.
     */
    uint8_t nrOfLeds() const {return m_NrOfLeds;}

    /*!
     * Get the Color of a LED for a given index.
     * \param inIndex the index of the LED to query the Color.
     * \return The Color of the LED adrresse by index \p inIndex
     */
    LEDColor& operator[](int16_t inIndex);

    /*!
     * Commit the canges to the LED Strip.
     * This method actually performs the transfer to the LED Strip.
     */
    void commit();

private:
    inline void _pulseStrobe(void);
}; // class LEDStrip
