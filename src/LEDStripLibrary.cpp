// File: LEDStripLibrary.cpp
#include "LEDStripLibrary.h"
#include <avr/interrupt.h>

#pragma GCC optimize ("O3")
/* ===================================================================
 * These two Functions shape the pulse for a WD2811 LED Driver.
 * Note that these are calculated for a Board which runs at a Clock Frequency
 * of 16 MHz!
 *
 * If your Board runs at another Clock then yo'll consider to cange these
 * two macros accordingly!
 * Not: It is vital that these two functions are compiled with optimization
 * turned on! That is because the set and clear mask  hast to be substituded
 * to ```sbi``` and ```cbi``` assembly instructions which will happen only of
 * optimization is active!
 * =================================================================== */
// A '0' is a pulse with a high time of 0.320 us and a low time of 0.8 us
inline static void LED_STRIP_PULSE0(volatile uint8_t& inPort, uint8_t inPortMask)
{
    // Turn on optimization to let the compiler create a
    // ```sbi _SFR_IO_ADDR(inPort),inPortBit```instruction!
    inPort |= inPortMask;
    __asm __volatile__(\
        /* 5 Clocks for high (0.375 us @ 16 MHz) */\
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
    );

    // Turn on optimization to let the compiler create a
    // ```cbi _SFR_IO_ADDR(inPort),inPortBit```instruction!
    inPort &= ~inPortMask;
    __asm __volatile__(\
        /* 14 Clocks for low (0.875 us @ 16 MHz) */\
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
    );
} __attribute__((optimize()))

// A '1' is a pulse with a high time of 0.65 us and a low time of 1 us
// A 'CAPT' is a pulse with a low time of > 50us
inline static void LED_STRIP_PULSE1(volatile uint8_t& inPort, uint8_t inPortMask)
{
    // Turn on optimization to let the compiler create a
    // ```sbi _SFR_IO_ADDR(inPort),inPortBit```instruction!
    inPort |= inPortMask;
    __asm __volatile__(\
        /* 10 Clocks for high (0.625 us @ 16 MHz) */\
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
        "   nop\n"     /* 1 Clock */ \
    );

    // Turn on optimization to let the compiler create a
    // ```cbi _SFR_IO_ADDR(inPort),inPortBit```instruction!
    inPort &= ~inPortMask;
    __asm __volatile__(\
        /* 6 Clocks for low (0.35 us @ 16 MHz) */\
        "   nop\n"     /* 1 Clock */ \
    );
} __attribute__((optimize()))

/*!
 * Convenient function that determines the ports corresponding DDR (Data Direction
 * Register) from a given Port Register.
 * \param inPort The Port Register to get the DDR register for.
 * \return the DDR register that belongs to the Port register \p inPort
 *
 * Example: DDR_FROM_PORT(PORTB) returns DDRB
 * \see PIN_FROM_PORT(volatile uint8_t&)
 */
inline static volatile uint8_t& DDR_FROM_PORT(volatile uint8_t& inPort)
{
    // The DDR Register is _one_ __before__ the PORT register
    return *((&inPort) - 1);
}

/*!
 * Convenient function that determines the ports corresponding PIN from
 * a given Port Register.
 * \param inPort The Port Register to get the PIN register for.
 * \return the DDR register that belongs to the Port register \p inPort
 *
 * Example: PIN_FROM_PORT(PORTB) returns PINB
 * \see PORT_FROM_PORT(volatile uint8_t&)
 */
inline static volatile uint8_t& PIN_FROM_PORT(volatile uint8_t& inPort)
{
    // The PIN Register is _two_ __before__ the PORT register
    return *((&inPort) - 2);
}

inline static bool isI_SREGSet0()
{
    uint8_t sreg;

    __asm __volatile__(
        "in %0,__SREG__"
        :"=r"(sreg)
    );
    return (sreg & _BV(7)); // Return whether the I-Flag (bit #7 of SREG) is set.
}

inline static void _shiftLEDColorValue(const LEDColor& inLEDColorValue, volatile uint8_t& inPort, uint8_t inPortMask)
{
    // The write Order is B->R->G
    const uint8_t g(inLEDColorValue.g);
    for (register uint16_t mask=1<<7; mask; mask>>=1)
    {
        if (g&mask) LED_STRIP_PULSE1(inPort, inPortMask);
        else        LED_STRIP_PULSE0(inPort, inPortMask);
    }

    const uint8_t r(inLEDColorValue.r);
    for (register uint16_t mask=1<<7; mask; mask>>=1)
    {
        if (r&mask) LED_STRIP_PULSE1(inPort, inPortMask);
        else        LED_STRIP_PULSE0(inPort, inPortMask);
    }

    const uint8_t b(inLEDColorValue.b);
    for (register uint16_t mask=1<<7; mask; mask>>=1)
    {
        if (b&mask) LED_STRIP_PULSE1(inPort, inPortMask);
        else        LED_STRIP_PULSE0(inPort, inPortMask);
    }
}

LEDStrip::LEDStrip(uint16_t inNrOfLeds, volatile uint8_t& inPort, uint8_t inPortPin)
: m_NrOfLeds(inNrOfLeds)
, m_Port(inPort)
, m_PortMask(1<<inPortPin)
, m_Leds((LEDColor*)::malloc(sizeof(LEDColor) * m_NrOfLeds))
{
    m_Port &= ~(m_PortMask);
    DDR_FROM_PORT(m_Port) = m_PortMask;

    for (register uint16_t i = 0; i != m_NrOfLeds; ++i)
    {
        m_Leds[i] = LEDColor(0,0,0);
    }
}

void LEDStrip::commit()
{
    ISRDisabler disabler; // Automatic Disable on ctor and disable it on dtor
    for (register uint16_t i = 0; i != nrOfLeds(); ++i)
    {
        _shiftLEDColorValue(m_Leds[i], m_Port, m_PortMask);
    }
    _pulseStrobe();
}

LEDStrip::~LEDStrip()
{
    free(m_Leds);
}

LEDColor& LEDStrip::operator[](int16_t inIndex)
{
    inIndex %= m_NrOfLeds;
    if (inIndex < 0)
    {
        inIndex += m_NrOfLeds;
    }

    return m_Leds[inIndex];
}

// ===============================================================================
// LEDStrip Private
// ===============================================================================
void LEDStrip::_pulseStrobe(void)
{
	_delay_us(50);
}
