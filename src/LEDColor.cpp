// File: LEDColor.cpp

// struct LEDColor
// ===================================================================
// LEDColor static
// ===================================================================
#include "LEDColor.h"

LEDColor LEDColor::colorWithHSV(int16_t inHue, uint8_t inSaturation, uint8_t inValue)
{
	LEDColor color;
	inHue %= 360;
	if (inHue < 0)
	{
	    inHue += 360;
	}

	const uint8_t c (uint16_t(uint16_t(inValue) * inSaturation) / 255);
	const uint8_t _hue120(inHue % 120);
	const uint8_t x(((_hue120 < 60) ? (c * _hue120) : (c * (120-_hue120))) / 60);

	if (inHue <= 60)
		color = LEDColor(c,x,0);
	else if (inHue <= 120)
		color = LEDColor(x,c,0);
	else if (inHue <= 180)
		color = LEDColor(0,c,x);
	else if (inHue <= 240)
		color = LEDColor(0,x,c);
	else if (inHue <= 300)
		color = LEDColor(x,0,c);
	else
		color = LEDColor(c,0,x);

	const uint8_t m (inValue - c);
	color.r += m;
	color.g += m;
	color.b += m;

	return color;
}

LEDColor mixedColor(const LEDColor& inColorA, const LEDColor& inColorB, uint8_t inWeighting)
{
    #define MIXED_COL(a,b,w) (((uint16_t(a)*(255-(w))) + (uint16_t(b)*(w)))/255)

    return LEDColor(
        MIXED_COL(inColorA.r, inColorB.r, inWeighting),
        MIXED_COL(inColorA.g, inColorB.g, inWeighting),
        MIXED_COL(inColorA.b, inColorB.b, inWeighting)
    );
}

// ===================================================================
// LEDColor private
// ===================================================================
void LEDColor::_limitBrightness(uint8_t inBrightValue)
{
	uint16_t sumBright(r+g+b);
	uint32_t averageBright((uint32_t(inBrightValue) * sumBright) / 3 );

	r = (uint16_t(r)*averageBright);
	g = (uint16_t(g)*averageBright);
	b = (uint16_t(b)*averageBright);
}
